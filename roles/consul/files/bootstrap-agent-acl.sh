#!/bin/bash

set -e

MASTER_TOKEN="$1"
AGENT_TOKEN="$2"
echo '
{
    "ID": "'"${AGENT_TOKEN}"'",
    "Name": "Agent Policy",
    "Rules": "{
        \"service\" : {
            \"\" : {
                \"policy\": \"write\"
            }
        },
        \"key\" : {
            \"\" : {
                \"policy\": \"write\"
            }
        }
    }"
}' | curl -X PUT -d @- http://consul.service.consul:8500/v1/acl/create?token=${MASTER_TOKEN}
