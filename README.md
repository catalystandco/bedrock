**bedrock** is Catalyst&Co's devops platform that makes deploying our art quick, painless, and easy.

Usage:

```bash
$ git clone http://bitbucket.org/catalystandco/bedrock
$ SSH_KEY=~/.ssh/amazon.pem ./launch.sh
```

Target Workflow:

 - Write code
 - Define service file: `bedrock.yml`
 - Enable repository on CI/CD service.
 - Push code to DVCS.
 - Code is pulled by CI/CD, built and then deployed onto bedrock.
 - bedrock reads service definition file, finds resources (i.e. GPUs, xGB of RAM), orchestrates dependencies and manages secrets, routing, security, etc.

Target Principles:

 - Deployable on any form of infrastructure (i.e. Public, private or hybird cloud)
 - Simple deployment (`brew install bedrock && bedrock launch`)
 - By default not high available, but can be made so easily. (`bedrock ha`)
 - Other cloud services are first class citizens (i.e. Amazon RDS is just a service)

Target features:

 - integrated with ACME SSL cert issuer like Lets Encrypt

Uses the power of:

- Terraform for infrastructure and service definition.
- HaProxy for routing
- Vault for secrets
- Ansible for server configuration management.
