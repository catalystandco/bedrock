variable "vpc_name" {
  default = "datarest-vpc"
}

variable "cidr_blocks" {
  default = "0.0.0.0/0"
  description = "CIDR for security group"

}

variable "sg_name" {
  default = "rds_sg"
  description = "Tag Name for security group"
}

variable "subnet_1_cidr" {
  default = "10.0.1.0/24"
}

variable "subnet_2_cidr" {
  default = "10.0.2.0/24"
}

variable "cluster_subnet_cidr" {
  default = "10.0.10.0/24"
}

variable "az_1" {
  default = "ap-southeast-2a"
  description = "Your Az1, use AWS CLI to find your account specific"
}

variable "az_2" {
  default = "ap-southeast-2b"
  description = "Your Az2, use AWS CLI to find your account specific"
}

