resource "aws_subnet" "D-SN-AU-C01" {
  vpc_id = "${aws_vpc.D-VPC-AU.id}"
  cidr_block = "10.0.10.0/24"
  availability_zone = "ap-southeast-2a"

  tags {
    Name = "D-SN-AU-C01"
  }
}

resource "aws_internet_gateway" "D-IG-AU-01" {
    vpc_id = "${aws_vpc.D-VPC-AU.id}"

    tags {
        Name = "D-IG-AU-01"
    }
}

resource "aws_route_table" "D-RT-AU-01" {
    vpc_id = "${aws_vpc.D-VPC-AU.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.D-IG-AU-01.id}"
    }

    tags {
        Name = "D-RT-AU-01"
    }
}

resource "aws_route_table_association" "eu-west-1a-public" {
  subnet_id = "${aws_subnet.D-SN-AU-C01.id}"
  route_table_id = "${aws_route_table.D-RT-AU-01.id}"
}

resource "aws_subnet" "D-SN-AU-RDS01" {
  vpc_id = "${aws_vpc.D-VPC-AU.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-southeast-2a"

  tags {
    Name = "D-SN-AU-RDS01"
  }
}

resource "aws_subnet" "D-SN-AU-RDS02" {
  vpc_id = "${aws_vpc.D-VPC-AU.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "ap-southeast-2b"

  tags {
    Name = "D-SN-AU-RDS02"
  }
}
