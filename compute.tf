resource "aws_instance" "D-C-AU-S01" {

  ami = "ami-949abdf7"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.D-SG-AU-CLUSTER.id}"]
  subnet_id = "${aws_subnet.D-SN-AU-C01.id}"
  key_name = "amazon"
  associate_public_ip_address = true

  tags {
    Name = "D-C-AU-S01"
    sshUser = "core"
    role = "control"
  }
}
