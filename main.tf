provider "aws" {
  region = "ap-southeast-2"
}

resource "aws_vpc" "D-VPC-AU" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "D-VPC-AU"
  }
}

resource "aws_security_group" "D-SG-AU-M" {
  name = "D-SG-AU-M"
  vpc_id = "${aws_vpc.D-VPC-AU.id}"

  ingress {
    from_port = 0
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "D-SG-AU-M"
  }
}

resource "aws_security_group" "D-SG-AU-CLUSTER" {
  name = "D-SG-AU-CLUSTER"
  vpc_id = "${aws_vpc.D-VPC-AU.id}"

  ingress {
    from_port = 0
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "D-SG-AU-CLUSTER"
  }
}

