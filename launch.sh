#!/bin/bash

set -e

SSH_KEY=${SSH_KEY:-"$HOME/.ssh/id_rsa"}

if [ ! -f "$SSH_KEY" ]; then
  mkdir $HOME/.ssh
fi

eval `ssh-agent -s` && ssh-add $SSH_KEY

python config.py
terraform apply
ansible-playbook playbooks/main.yml --extra-vars=@config.yml --private-key $SSH_KEY
